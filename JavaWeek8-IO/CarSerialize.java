
// File processing with OBJECTS
// use of ObjectOutputStream and ObjectInputStream classes

// Question: What if the class Car does not implement the Serializable interface?

import java.io.*;

public class CarSerialize{

   public static void main(String[] args) {


	try {

			// Part 1
            Car c, d;
            c = new Car( "Toyota Corolla", "ABCD765", 3500 );

 	     	System.out.println( "writing a car object to the file" );

	     	ObjectOutputStream out = new ObjectOutputStream(
                                                  new FileOutputStream( "car.dat" ) );

            out.writeObject( c );	// write an object to the file
            out.close();


	     	ObjectInputStream  in = new ObjectInputStream(
                                                   new FileInputStream( "car.dat" ) );

            d = (Car) in.readObject();  // casting!!!	// read an object from the file
            in.close();

            System.out.println( "one object read from the file:\n" + d );


             // Part 2: write an array of objects
            System.out.println( "\n******* Read/Write an array of TWO objects *******" );

            Car[]   z = new Car[2];

            z[0] = new Car( "Honda Civic", "XYZ007", 5555 );
	     	z[1] = c;


            out = new ObjectOutputStream(
                                   new FileOutputStream( "vehicles.dat" ) );

            out.writeObject( z );   // an array is an object!
                                    // write an array of objects to the file
            out.close();

	     	in = new ObjectInputStream(
                                 new FileInputStream( "vehicles.dat" ) );

            Car[] w = (Car[]) in.readObject();  // casting!!!

            for(int i=0; i<2; i++)

 					System.out.println( w[i] );

            in.close();
       }
       catch( ClassNotFoundException cnf) { // exception thrown by readObject()

                                            cnf.printStackTrace(); }

       catch( IOException e) { e.printStackTrace(); }

   } // end main
}

/* error message

writing a car object to the file
java.io.NotSerializableException: Car
        at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1081)

        at java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:302)
        at CarSerialize.main(CarSerialize.java:25)
*/