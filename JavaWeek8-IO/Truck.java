// the class implements the Serializable interface

import java.io.*;

public class Truck extends Car
                  implements Serializable {

   private int weight;

   public Truck( String brand, String name, double k, int w ) {
      super( brand, name, k);
      weight = w;
   }

   public String toString() {

      String s = "";

      s += super.toString();
      s += " weight: " + weight;

      return s;
   }

}
