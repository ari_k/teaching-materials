// Source: Java Tutorial
// Modified by Peter Liu

// character/text File I/O
// use of FileReader, FileWriter

import java.io.*;

public class FileReaderWriter {

    public static void main(String[] args) throws IOException {
	
	/* IOException is thrown for a failed or interrupted I/O */
        /*    subclass: FileNotFoundException                    */

	/* File: the description of the properties of a disk file

	   File inputFile = new File("farrago.txt");
	   File outputFile = new File("outagain.txt");
         */

        /* FileReader: a Reader(i.e. character-based stream) used to read
                       the contents of a file (a data sink stream)
           FileWriter: a Writer used to write to a file (a data sink stream)
         */

        String inputFile  = "farrago.txt";	// human-readable
        String outputFile = "outagain.txt";     // human-readable!

        FileReader in  = new FileReader( inputFile );
        FileWriter out = new FileWriter( outputFile );

        int c;

        while ((c = in.read()) != -1)   // read a character
					// end of file: -1
        {
           out.write(c);		// write a character

           System.out.print( (char)c );       /* for testing purpose */
        }

        in.close();
        out.close();
    }
}
