
// File processing with OBJECTS
// use of ObjectOutputStream and ObjectInputStream classes

// Question: What if the class Car does not implement the Serializable interface?

import java.io.*;

public class AListSerialize{

   public static void main(String[] args) {


	try {

			// Part 1
	     	ObjectOutputStream outFile = new ObjectOutputStream(
                                                  new FileOutputStream( "list.dat" ) );



			Car [] v = new Car [2];

            // build a list of 2 cars
            Car c, d;

			c = new Car( "Toyota Corolla", "ABCD765", 3500 );
			d = new Car( "Honda Civic", "XYZ9854", 111 );

			v[0] = c;
			v[1] = d;


			System.out.println( "... writing an ArralyList object to a file" );
            outFile.writeObject( v );	// write an object to the file
            outFile.close();


			// Part 2
			ObjectInputStream  inFile = new ObjectInputStream(
                                                  new FileInputStream( "list.dat" ) );

            Car [] back;

            back = (Car []) inFile.readObject();

            System.out.println( "... reading an ArrayList object from the file:\n" );

 			for (int k=0; k<back.length; k++)

 			     System.out.println( v[k] );

            inFile.close(); // close the file
       }
       catch( ClassNotFoundException cnf) { // exception thrown by readObject()

                                            cnf.printStackTrace(); }

       catch( IOException e) { e.printStackTrace(); }

       catch( Exception cce ) { cce.printStackTrace(); }

   } // end main
}

/* error message:

... writing an ArralyList object to a file
java.io.NotSerializableException: Car
        at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1156)

        at java.io.ObjectOutputStream.writeArray(ObjectOutputStream.java:1338)
        at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1146)

        at java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:326)
        at AListSerialize.main(AListSerialize.java:35)

*/