// the class implements the Serializable interface

// this class is used by ExceptionVersion

import java.io.*;

public class Car2 implements Serializable {

   private String model;
   private String owner;
   private double mileage;
   private String registration;

   public Car2( String brand, String name, double k ) {
      model = brand;
      owner = name;
      mileage = k;
      registration = "xxx";
   }

   public String toString() {

      String s = "";

      s += "Model: " + model + " Owner: " + owner +
           " mileage: " +  mileage +
           " Registration: " + registration;

      return s;
   }

   public void getRegistered(String num) {
      registration = num;
   }

   //public void dummy() { }  // used just to change the fingerprint of the class

}
