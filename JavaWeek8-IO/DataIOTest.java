// Source: Java Tutorial
// Modified by Peter Liu

// File I/O: use byte streams 
//	     to read and write primitive data types

import java.io.*;

public class DataIOTest {

    public static void main(String[] args) throws IOException {

        // Part 1: write the data out

 	/* DataOutputStream(processsing stream): 
              an OutputStream object used for writing the Java standard types 
              to a byte stream( primitive data types => bytes )

	   FileOutputStream(data sink stream): 
              an OutputStream object that writes bytes to a file( bytes => disk data )
         */

        DataOutputStream out = new DataOutputStream(
                                      new FileOutputStream("invoice1.dat") );
					              // the file is binary: NOT human-readable!

        double[]  prices = { 19.99, 9.99, 15.99, 3.99, 4.987654 };
        int[]     units  = { 12, 8, 13, 29, 50 };
        String[]  descs  = { "Java T-shirt",
			     "Java Mug",
			     "Duke Juggling Dolls",
			     "Java Pin",
			     "Java Key Chain" };
        
        for (int i = 0; i < prices.length; i ++) {

            /* specialized write methods */
            out.writeDouble(prices[i]);
            out.writeChar('\t');	   

            out.writeInt(units[i]);
            out.writeChar('\t');	  

            out.writeUTF(descs[i]);        // write a string
            out.writeChar('\n');
        }
        out.close();

        System.out.println( "*** the program is done with writing to a binary file" );


        // Part 2: read it in again

        /* FileInputStream(data sink stream): 
              an InputStream object for reading bytes from a file

	   DataInputStream(processing stream): 
              an InputStream object used for reading the Java standard data types
              from a byte stream( bytes => primitive date types )
         */

        DataInputStream in = new DataInputStream(
                                    new FileInputStream("invoice1.dat"));

        double price;
        int    unit;
        String desc;
        double total = 0.0;

        System.out.println( "*** reading the binary file..." ); 

        try {
            while (true) {

		/* specialized read methods */
                price = in.readDouble();
                in.readChar();             // throw out the tab

                unit = in.readInt();
                in.readChar();            // throw out the tab

                desc = in.readUTF();	  // read a string
                in.readChar();            // throw out '\n'

                System.out.println("You've ordered " +
				    unit + " units of " +
				    desc + " at $" + price);

                total = total + unit * price;
            }

        } catch (EOFException e) {}  // API: end of file condition or
                                     //        end of stream condition

        System.out.println("For a TOTAL of: $" + total);
			              // How to control the format?

        in.close();
    }
}
