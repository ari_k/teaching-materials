
// Truck is a subclass of Car

import java.io.*;

public class TruckSerialize{

   public static void main(String[] args) {


	try {

			// Part 1
            Car c, d;
            c = new Car( "Toyota Corolla", "ABCD765", 3500 );

            Truck z = new Truck( "GM", "XYZ456", 3500, 9 );

            System.out.println( c.toString() + z.toString() );


 	     	System.out.println( "writing a Car object and a Truck object to the file" );

	     	ObjectOutputStream out = new ObjectOutputStream(
                                                  new FileOutputStream( "cartruck.dat" ) );

            out.writeObject( c );	// write an object to the file
            out.writeObject( z );
            out.close();


	     	ObjectInputStream  in = new ObjectInputStream(
                                                   new FileInputStream( "cartruck.dat" ) );

            Car x;
            //Truck y;
            Car y;

            x = (Car) in.readObject();  // casting!!!	// read an object from the file
            y = (Car) in.readObject();
            							// y = (Truck) in.readObject();

            in.close();

            System.out.println( "objects read from the file:\n" + x + y );
       }
       catch( ClassNotFoundException cnf) { // exception thrown by readObject()

                                            cnf.printStackTrace(); }

       catch( IOException e) { e.printStackTrace(); }

   } // end main
}

/* error message

writing a car object to the file
java.io.NotSerializableException: Car
        at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1081)

        at java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:302)
        at CarSerialize.main(CarSerialize.java:25)
*/