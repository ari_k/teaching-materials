// console-based input/output

import java.io.*;    // BufferedReader, IOException
import java.util.*;  // StringTokenizer

public class ConsoleApp {

   public static void main(String[] args)
                          throws IOException
   {
	System.out.println( "Enter a line of characters:" ); 

	// System.in: 	      an InputStream object connceted to the keyboard 
	//                    (a data sink stream)
	// InputStreamReader: convert a byte stream into a character stream
	//                    (a processing stream)
	// BufferedReader:    a buffered character stream that reads a character or a string
        //                    methods: read(), readLine()
	//                    (a processing stream)
	// programming approach: wrap System.in by "connecting streams"

	InputStreamReader isr =    new InputStreamReader( System.in );
	BufferedReader    reader = new BufferedReader( isr );
	
		/* BufferedReader reader = new BufferedReader(
                                               new InputStreamReader( System.in) ));
                 */

	String inputLine = reader.readLine();

	System.out.println( "You have entered:\n" + inputLine );


	// Part 2: Interactive I/O with the keyboard
	//         Read data of different types

        /* input format: int, float, string */
	System.out.println( "*** Enter an integer, a float and a string: " );
	
	inputLine = reader.readLine();  // returns null at the end of stream

        // use the nextToken() method to extract nonspace characters
	StringTokenizer chopper = new StringTokenizer( inputLine );

	// use parseXXX method

	int m = Integer.parseInt( chopper.nextToken() );    // extract an int

	// Java 2
	float z = Float.parseFloat( chopper.nextToken() );  // extract a float

	      /* JDK 1.1
	         valueOf(): convert a String object into a Float object
	         float z = Float.valueOf( chopper.nextToken() ).floatValue();
               */
	
	String remaining = chopper.nextToken();  // extract another token: a string
	
	while ( chopper.hasMoreTokens() ) {
		remaining += "*" + chopper.nextToken();
	}


 	// System.out: a PrintStream object connected to the console
        //             methods: print(), println()
        //             (a data sink stream)

	System.out.println( "integer: " + m );
	System.out.println( "float: " + z );
	System.out.println( "string: " + remaining );

       
   }
}