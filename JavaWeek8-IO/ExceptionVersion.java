
// File processing with OBJECTS
// use of ObjectOutputStream and ObjectInputStream classes

// Question: What if the class Car2 has been modifed after the file "car2object.dat" is created?

import java.io.*;
import java.util.Vector;

public class ExceptionVersion{

   public static void main(String[] args) {


	try {

			// Part 1
	     	/*ObjectOutputStream out = new ObjectOutputStream(
                                                  new FileOutputStream( "car2object.dat" ) );

             */
            ObjectInputStream  in = new ObjectInputStream(
                                                  new FileInputStream( "car2object.dat" ) );
/*
			Vector v = new Vector();

            Car2 c, d;

			c = new Car2( "Toyota Corolla", "ABCD765", 3500 );
			d = new Car2( "Honda Civic", "XYZ9854", 111 );

			Vector z = new Vector();
			z.addElement( "12345" );
			z.addElement( "010101" );
			z.addElement( "!@#$%" );

            // add a vector of 3 strings to the vector v
			v.addElement( z );

			System.out.println( "writing objects to the file..." );
			out.writeObject( c );
			out.writeObject( d );
            out.writeObject( v );	// write an object to the file
            out.close();
*/

			// Part 2
               Car2 back = (Car2) in.readObject();   // the class has been modified between
                                                // writeObject() and readObject()

               System.out.println( "... reading done" );

               in.close();


       }
       catch( ClassNotFoundException cnf) { // exception thrown by readObject()

                                            cnf.printStackTrace(); }

       catch( IOException e) { e.printStackTrace(); }

       // EOFException is thrown when the end of the stream is reached

   } // end main
}

/*
error message:

java.io.InvalidClassException: Car2; local class incompatible: stream classdesc
serialVersionUID = 4897792221646040248, local class serialVersionUID = 234094196
0801072135
        at java.io.ObjectStreamClass.initNonProxy(ObjectStreamClass.java:546)
        at java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:155
2)
        at java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1466)
        at java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:1
699)
        at java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1305)
        at java.io.ObjectInputStream.readObject(ObjectInputStream.java:348)
        at ExceptionVersion.main(ExceptionVersion.java:48)

*/