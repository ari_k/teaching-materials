// use of byte streams for file input/output

import java.io.*;    // BufferedReader, IOException
import java.util.*;  // StringTokenizer

public class FileStreams {

   public static void main(String[] args)
                          throws IOException {

	// FileInputStream:   a byte stream object connected to a physical file
	//                    (a data sink stream: source of data transfer)
	// InputStreamReader: convert a byte stream into a character stream
	//                    (a processing stream)
	// BufferedReader:    a buffered character stream that reads a character or a string
        //                    methods: read(), readLine()
	//                    (a processing stream)

	// PrintWriter:	      a buffered character stream
	//                    an intermediate OutputStreamWriter is used to
	//                    convert characters into bytes
	// FileOutputStream:  a byte stream object connected to a physical fle
	//		      (a data sink stream: destination of data transfer)



	BufferedReader reader = new BufferedReader(
                                    new InputStreamReader(
					new FileInputStream( "farrago2.txt" )
				));

	PrintWriter writer = new PrintWriter(
				 new FileOutputStream( "output2.txt" ) );

	boolean exit = false;

	while ( !exit ) {

	   String inputLine = reader.readLine();

	   if ( inputLine != null ) {

		System.out.println( "a line from the file: " + inputLine );

		writer.println( inputLine );
		writer.flush();			// flush the buffer
	   }

	   else
		exit = true;
	}
	reader.close();
	writer.close();
   }
}