package com.daily.workout.app;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class Ads {
	static void loadAds(AdView ads){
		
		/**
		 * beginning of admob testing code
		 * 
		 * this code is for testing admob only, 
		 * when you publish the app remove the following code
		 */
		AdRequest adRequest = new AdRequest();
		// use this code below to test admob on emulator
		//adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
		
		// use this code below to test admob on android device
		adRequest.addTestDevice("DEVICE_ID");
		ads.loadAd(adRequest);
		/**
		 * end of admob testing code
		 */
		
		/**
		* code below is used to publish admob when the app launched.
		* remove the comment tag below and delete block of code 
		* that used for testing admob.
		*/

		//ads.loadAd(new AdRequest());
	}
}
